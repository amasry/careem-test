package myapplication.careemtest;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.squareup.picasso.Picasso;
import myapplication.careemtest.objects.DetailedMovie;
import rx.functions.Action1;

/**
 * Created by Masry on 06/11/2016.
 */

public class DetailsActivity extends Activity {

  public static final String MOVIE_ID_EXTRA = "movie_id";

  TextView mTitle;
  TextView mOriginalTitle;
  TextView mOverView;
  TextView mReleaseDate;
  ImageView mMainImage;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_details);

    mTitle = (TextView) findViewById(R.id.title);
    mOriginalTitle = (TextView) findViewById(R.id.original_title);
    mOverView = (TextView) findViewById(R.id.overview);
    mReleaseDate = (TextView) findViewById(R.id.release_date);
    mMainImage = (ImageView) findViewById(R.id.main_image);

    int movieId = getIntent().getIntExtra(MOVIE_ID_EXTRA, 0);
    MoviesManager.getDetailedMovieObservable(this, movieId).subscribe(new Action1<DetailedMovie>() {
      @Override
      public void call(DetailedMovie detailedMovie) {
        mTitle.setText(detailedMovie.getTitle());
        mOriginalTitle.setText(detailedMovie.getOriginalTitle());
        mOverView.setText(detailedMovie.getOverview());
        mReleaseDate.setText(detailedMovie.getReleaseDate());
        Picasso.with(DetailsActivity.this)
            .load(MoviesManager.getBigPosterFullUrl(detailedMovie.getPosterPath()))
            .into(mMainImage);
      }
    }, throwable -> {
      Toast.makeText(DetailsActivity.this, throwable.getMessage(), Toast.LENGTH_LONG).show();
    });
  }
}
