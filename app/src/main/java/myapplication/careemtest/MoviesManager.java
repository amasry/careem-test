package myapplication.careemtest;

import android.content.Context;
import myapplication.careemtest.networking.ApiManager;
import myapplication.careemtest.networking.MoviesService;
import myapplication.careemtest.networking.ResponseHandler;
import myapplication.careemtest.objects.DetailedMovie;
import myapplication.careemtest.objects.PopularMovies;
import retrofit2.Retrofit;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Masry on 06/11/2016.
 */

public class MoviesManager {

  private static final String API_KEY = "13f82de5311770c7e57a76540cc90be3";

  private static final String LANGUAGE = "en-US";

  private static final String POSTERS_BASE_URL = "http://image.tmdb.org/t/p/";

  private static final String POSTER_BIG_SIZE = "w500/";

  private static final String POSTER_SMALL_SIZE = "w342/";

  public static Observable<PopularMovies> getMoviesObservable(Context context, int page) {
    return getMoviesService(context).getPopularMovies(API_KEY, LANGUAGE, page+"")
        .flatMap(new ResponseHandler<PopularMovies>()::handleResponse)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread());
  }

  public static Observable<DetailedMovie> getDetailedMovieObservable(Context context, int id) {
    return getMoviesService(context).getMovieDetails( id+"", API_KEY, LANGUAGE)
        .flatMap(new ResponseHandler<DetailedMovie>()::handleResponse)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread());
  }

  private static MoviesService getMoviesService(Context context) {
    Retrofit retrofit = ApiManager.getRetrofit(context);
    return retrofit.create(MoviesService.class);
  }

  public static String getBigPosterFullUrl(String path) {
    return getPosterFullUrl(path, POSTER_BIG_SIZE);
  }

  public static String getSmallPosterFullUrl(String path) {
    return getPosterFullUrl(path, POSTER_SMALL_SIZE);
  }
  private static String getPosterFullUrl(String path, String size) {
    return String.format("%s%s%s", POSTERS_BASE_URL, size, path);
  }
}
