package myapplication.careemtest;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener,
    AbsListView.OnScrollListener {

  private MoviesListAdapter mAdapter;

  private int mPage = 1;

  ListView mListView;
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    mListView = (ListView) findViewById(R.id.main_lv);
    mListView.setOnItemClickListener(this);
    mListView.setOnScrollListener(this);

    loadMovies();
  }

  private void loadMovies() {
    MoviesManager.getMoviesObservable(this, mPage).subscribe(popularMovies -> {
      if (mAdapter == null) {
        mAdapter = new MoviesListAdapter(popularMovies.getResults(), MainActivity.this);
        mListView.setAdapter(mAdapter);
      } else {
        mAdapter.addAll(popularMovies.getResults());
        mAdapter.notifyDataSetChanged();
      }
      mPage++;
    }, throwable -> {
      Toast.makeText(MainActivity.this, throwable.getMessage(), Toast.LENGTH_LONG).show();
    });
  }

  @Override
  public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    Intent intent = new Intent(this, DetailsActivity.class);
    intent.putExtra(DetailsActivity.MOVIE_ID_EXTRA, mAdapter.getItem(position).getId().intValue());
    startActivity(intent);
  }

  @Override
  public void onScrollStateChanged(AbsListView view, int scrollState) {

  }

  @Override
  public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount,
      int totalItemCount) {
    int lastVisibleItem = firstVisibleItem + visibleItemCount;

    if (totalItemCount != 0
        && lastVisibleItem == totalItemCount) {
      loadMovies();
    }
  }
}
