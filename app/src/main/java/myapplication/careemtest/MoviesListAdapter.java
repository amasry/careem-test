package myapplication.careemtest;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.List;
import myapplication.careemtest.objects.Movie;

/**
 * Created by Masry on 06/11/2016.
 */

public class MoviesListAdapter extends ArrayAdapter<Movie> {

  public MoviesListAdapter(List<Movie> objects, Activity activity) {
    super(activity, R.layout.movies_list_item, objects);
  }

  @NonNull
  @Override
  public View getView(int position, View convertView, ViewGroup parent) {
    ViewHolder viewHolder;
    if (convertView == null) {
      convertView =
          LayoutInflater.from(getContext()).inflate(R.layout.movies_list_item, parent, false);
      viewHolder = new ViewHolder();
      viewHolder.mMainImage = (ImageView) convertView.findViewById(R.id.main_image);
      viewHolder.mTitle = (TextView) convertView.findViewById(R.id.title);
      viewHolder.mReleaseDate = (TextView) convertView.findViewById(R.id.release_date);
      convertView.setTag(viewHolder);
    } else {
      viewHolder = (ViewHolder) convertView.getTag();
    }
    Movie movie = getItem(position);
    viewHolder.mTitle.setText(movie.getTitle());
    viewHolder.mReleaseDate.setText(movie.getReleaseDate());
    Picasso.with(getContext())
        .load(MoviesManager.getSmallPosterFullUrl(movie.getPosterPath()))
        .into(viewHolder.mMainImage);
    return convertView;
  }

  class ViewHolder {
    ImageView mMainImage;
    TextView mTitle;
    TextView mReleaseDate;
  }
}
