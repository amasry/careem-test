package myapplication.careemtest.networking;

import android.content.Context;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Masry on 06/11/2016.
 */

public class ApiManager {

  private static Retrofit retrofit;

  private static final String DEFAULT_URL = " https://api.themoviedb.org/";

  public static Retrofit getRetrofit(Context context) {
    if (retrofit == null) {
      init(context, HttpUrl.parse(DEFAULT_URL));
    }
    return retrofit;
  }

  public static void init(Context context, HttpUrl baseUrl) {

    OkHttpClient client = new OkHttpClient.Builder().build();

    retrofit = new Retrofit.Builder().addCallAdapterFactory(RxJavaCallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .client(client)
        .baseUrl(baseUrl)
        .build();
  }
}
