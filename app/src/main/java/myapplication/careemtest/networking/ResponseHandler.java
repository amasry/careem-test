package myapplication.careemtest.networking;

import retrofit2.Response;
import rx.Observable;

/**
 * Created by Masry on 07/08/16.
 *For handling retrofit responses
 */

public class ResponseHandler<R> {

  /**
   *
   * @param response retrofit response
   * @return response body if successful, error otherwise
   */
  public Observable<R> handleResponse(Response<R> response) {
    if (response.isSuccessful()) {
      return Observable.just(response.body());
    } else {
      return Observable.error(new Throwable(response.message()));
    }
  }
}
