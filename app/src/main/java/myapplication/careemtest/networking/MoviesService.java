package myapplication.careemtest.networking;

import myapplication.careemtest.objects.DetailedMovie;
import myapplication.careemtest.objects.PopularMovies;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Masry on 06/11/2016.
 */

public interface MoviesService {

  @GET("/3/movie/popular")
  Observable<Response<PopularMovies>> getPopularMovies(@Query("api_key") String apiKey,
      @Query("language") String language, @Query("page") String page);

  @GET("https://api.themoviedb.org/3/movie/{id}")
  Observable<Response<DetailedMovie>> getMovieDetails(@Path("id") String movieId,
      @Query("api_key") String apiKey, @Query("language") String language);
}
